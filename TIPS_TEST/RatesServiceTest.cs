﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Configurations;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Logics;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Models;
using TIPS_CALCULATOR.Services;
using TIPS_CALCULATOR.Services.Interfaces;
using Xunit;

namespace TIPS_TEST
{
    public class RatesServiceTest
    {
        [Fact]
        public void TestGetRatesOk()
        {
            Mock<ILogger<RatesService>> loggerMock = new Mock<ILogger<RatesService>>();
            IParseToRate parseToRate = new ParseToRate();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new OkObjectResult("[{\"from\": \"USD\", \"to\": \"EUR\", \"rate\": 1.04}]") as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();

            RatesService service = new RatesService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToRate);

            var response = service.getRates().Result as OkObjectResult;
            var result = response.Value as List<Rate>;
            Rate rate = new Rate { from = "USD", to = "EUR", rate = 1.04m };

            Assert.Equal(rate.from, result[0].from);
        }

        [Fact]
        public void TestGetRatesBackup()
        {
            Mock<ILogger<RatesService>> loggerMock = new Mock<ILogger<RatesService>>();
            IParseToRate parseToRate = new ParseToRate();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("[{\"from\": \"USD\", \"to\": \"EUR\", \"rate\": 1.04}]");

            RatesService service = new RatesService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToRate);

            var response = service.getRates().Result as OkObjectResult;
            var result = response.Value as List<Rate>;
            Rate rate = new Rate { from = "USD", to = "EUR", rate = 1.04m };

            Assert.Equal(rate.from, result[0].from);
        }

        [Fact]
        public void TestGetRatesError()
        {
            Mock<ILogger<RatesService>> loggerMock = new Mock<ILogger<RatesService>>();
            IParseToRate parseToRate = new ParseToRate();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("ERROR");

            RatesService service = new RatesService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToRate);

            var response = service.getRates().Result as BadRequestResult;
            Assert.Equal(400, response.StatusCode);
        }
    }
}
