﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Configurations;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Logics;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Models;
using TIPS_CALCULATOR.Services;
using TIPS_CALCULATOR.Services.Interfaces;
using Xunit;

namespace TIPS_TEST
{
    public class TransactionsServiceTest
    {
        [Fact]
        public void TestGetTransactionsOk()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new OkObjectResult("[{\"sku\": \"AA2223\", \"amount\": 10.34, \"currency\": \"EUR\"}]") as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactions().Result as OkObjectResult;
            var result = response.Value as List<Transaction>;
            Transaction trans = new Transaction { sku = "AA2223", amount = 10.34m, currency = "EUR" };

            Assert.Equal(trans.sku, result[0].sku);
            Assert.Equal(trans.currency, result[0].currency);
            Assert.Equal(trans.amount, result[0].amount);
        }

        [Fact]
        public void TestGetTransactionsBackup()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("[{\"sku\": \"AA2223\", \"amount\": 10.34, \"currency\": \"EUR\"}]");

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactions().Result as OkObjectResult;
            var result = response.Value as List<Transaction>;
            Transaction trans = new Transaction { sku = "AA2223", amount = 10.34m, currency = "EUR" };

            Assert.Equal(trans.sku, result[0].sku);
            Assert.Equal(trans.currency, result[0].currency);
            Assert.Equal(trans.amount, result[0].amount);
        }

        [Fact]
        public void TestGetTransactionsError()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("ERROR");

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactions().Result as BadRequestResult;
            Assert.Equal(400, response.StatusCode);
        }

        [Fact]
        public void TestGetTransactionsBySkuOk()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new OkObjectResult("[{\"sku\": \"AA2223\", \"amount\": 10.34, \"currency\": \"EUR\"}]") as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactionsBySku("AA2223").Result as OkObjectResult;
            var result = response.Value as List<Transaction>;
            Transaction trans = new Transaction { sku = "AA2223", amount = 10.34m, currency = "EUR" };

            Assert.Equal(trans.sku, result[0].sku);
            Assert.Equal(trans.currency, result[0].currency);
            Assert.Equal(trans.amount, result[0].amount);
        }

        [Fact]
        public void TestGetTransactionsBySkuOKFoundInBackup()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new OkObjectResult("[{\"sku\": \"AA222543\", \"amount\": 10.34, \"currency\": \"EUR\"}]") as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("[{\"sku\": \"AA2223\", \"amount\": 10.34, \"currency\": \"EUR\"}]");

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactionsBySku("AA2223").Result as OkObjectResult;
            var result = response.Value as List<Transaction>;
            Transaction trans = new Transaction { sku = "AA2223", amount = 10.34m, currency = "EUR" };

            Assert.Equal(trans.sku, result[0].sku);
            Assert.Equal(trans.currency, result[0].currency);
            Assert.Equal(trans.amount, result[0].amount);
        }

        [Fact]
        public void TestGetTransactionsBySkuNotFoundNobackup()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new OkObjectResult("[{\"sku\": \"AA222543\", \"amount\": 10.34, \"currency\": \"EUR\"}]") as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("[{\"sku\": \"AA222354\", \"amount\": 10.34, \"currency\": \"EUR\"}]");

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactionsBySku("AA2223").Result as OkObjectResult;
            var result = response.Value as String;
            Assert.Equal(null, result);
        }

        [Fact]
        public void TestGetTransactionsBySkuBackup()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("[{\"sku\": \"AA2223\", \"amount\": 10.34, \"currency\": \"EUR\"}]");

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactionsBySku("AA2223").Result as OkObjectResult;
            var result = response.Value as List<Transaction>;
            Transaction trans = new Transaction { sku = "AA2223", amount = 10.34m, currency = "EUR" };

            Assert.Equal(trans.sku, result[0].sku);
            Assert.Equal(trans.currency, result[0].currency);
            Assert.Equal(trans.amount, result[0].amount);
        }
        
        [Fact]
        public void TestGetTransactionsBySkuError()
        {
            Mock<ILogger<TransactionsService>> loggerMock = new Mock<ILogger<TransactionsService>>();
            IParseToTransaction parseToTransactions = new ParseToTransaction();

            Mock<IClientService> clientMock = new Mock<IClientService>();
            clientMock.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            Mock<IConfiguration> configMock = new Mock<IConfiguration>();
            configMock.Setup(x => x.GetSection(It.IsAny<string>()).Value).Returns("x");
            IServiceConfig serviceConf = new ServiceConfig(configMock.Object);

            Mock<IBackup> backupMock = new Mock<IBackup>();
            backupMock.Setup(x => x.readBackup()).Returns("ERROR");

            TransactionsService service = new TransactionsService(clientMock.Object, loggerMock.Object, serviceConf, backupMock.Object, parseToTransactions);

            var response = service.getTransactionsBySku("AA2223").Result as BadRequestResult;
            Assert.Equal(400, response.StatusCode);
        }
    }
}
