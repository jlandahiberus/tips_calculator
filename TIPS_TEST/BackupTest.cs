﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System.IO;
using TIPS_CALCULATOR.Configurations;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Logics.Interfaces;
using Xunit;

namespace TIPS_TEST
{
    public class BackupTest
    {
        [Fact]
        public void TestWrite()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings-test.json").Build();
            IBackupConfig backupConfig = new BackupConfig(config);
            Mock<ILogger<Backup>> loggerMock = new Mock<ILogger<Backup>>();

            backupConfig.setConfigSection("test");
            Backup backup = new Backup(backupConfig, loggerMock.Object);

            if (Directory.Exists("C:/tmp/test/"))
            {
                Directory.Delete("C:/tmp/test/");
            }

            backup.writeBackup("TEST");
            string lectura = backup.readBackup();

            Assert.Equal("TEST", lectura);
        }

        [Fact]
        public void TestNotExist()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings-test.json").Build();
            IBackupConfig backupConfig = new BackupConfig(config);
            Mock<ILogger<Backup>> loggerMock = new Mock<ILogger<Backup>>();

            backupConfig.setConfigSection("test");
            Backup backup = new Backup(backupConfig, loggerMock.Object);

            if (File.Exists("C:/tmp/test/test.bak"))
            {
                File.Delete("C:/tmp/test/test.bak");
            }

            string lectura = backup.readBackup();
            Assert.Equal("ERROR", lectura);
        }
    }
}
