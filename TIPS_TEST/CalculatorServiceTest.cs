﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Logics;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Models;
using TIPS_CALCULATOR.Services;
using TIPS_CALCULATOR.Services.Interfaces;
using Xunit;

namespace TIPS_TEST
{
    public class CalculatorServiceTest
    {
        [Fact]
        public void TestAllOk()
        {
            List<Rate> rates = new List<Rate>();
            rates.Add(new Rate { from = "USD", to = "EUR", rate = 1.04m });
            List<Transaction> transactions = new List<Transaction>();
            transactions.Add(new Transaction { sku = "AA2223", amount = 10.34m, currency = "USD" });

            Mock<ILogger<CalculatorService>> loggerMock = new Mock<ILogger<CalculatorService>>();
            Mock<IRatesService> ratesMock = new Mock<IRatesService>();
            ratesMock.Setup(x => x.getRates())
                .Returns(Task.FromResult(new OkObjectResult(rates) as IActionResult));
            Mock<ITransactionsService> transMock = new Mock<ITransactionsService>();
            transMock.Setup(x => x.getTransactionsBySku(It.IsAny<string>()))
                .Returns(Task.FromResult(new OkObjectResult(transactions) as IActionResult));


            ITipCalculation tipCalculation = new TipCalculation();
            ICurrencyConversor currencyConversor = new CurrencyConversor();

            CalculatorService service = new CalculatorService(loggerMock.Object, ratesMock.Object, transMock.Object, tipCalculation, currencyConversor);
            var response = service.callService("AA2223", "EUR").Result as OkObjectResult;
            var result = response.Value as List<Tip>;

            Assert.Equal("TOTAL", result[0].sku);
            Assert.Equal("EUR", result[0].currency);
            Assert.Equal(10.75m, result[0].amount);
            Assert.Equal(0.54m, result[0].tip);
            

        }

        [Fact]
        public void TestTransactionsError()
        {
            List<Rate> rates = new List<Rate>();
            rates.Add(new Rate { from = "USD", to = "EUR", rate = 1.04m });

            Mock<ILogger<CalculatorService>> loggerMock = new Mock<ILogger<CalculatorService>>();
            Mock<IRatesService> ratesMock = new Mock<IRatesService>();
            ratesMock.Setup(x => x.getRates())
                .Returns(Task.FromResult(new OkObjectResult(rates) as IActionResult));
            Mock<ITransactionsService> transMock = new Mock<ITransactionsService>();
            transMock.Setup(x => x.getTransactionsBySku(It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            ITipCalculation tipCalculation = new TipCalculation();
            ICurrencyConversor currencyConversor = new CurrencyConversor();

            CalculatorService service = new CalculatorService(loggerMock.Object, ratesMock.Object, transMock.Object, tipCalculation, currencyConversor);
            var response = service.callService("AA2223", "EUR").Result as BadRequestResult;

            Assert.Equal(400, response.StatusCode);
        }

        [Fact]
        public void TestRatesError()
        {
            Mock<ILogger<CalculatorService>> loggerMock = new Mock<ILogger<CalculatorService>>();
            Mock<IRatesService> ratesMock = new Mock<IRatesService>();
            ratesMock.Setup(x => x.getRates())
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));
            Mock<ITransactionsService> transMock = new Mock<ITransactionsService>();
            transMock.Setup(x => x.getTransactionsBySku(It.IsAny<string>()))
                .Returns(Task.FromResult(new BadRequestResult() as IActionResult));

            ITipCalculation tipCalculation = new TipCalculation();
            ICurrencyConversor currencyConversor = new CurrencyConversor();

            CalculatorService service = new CalculatorService(loggerMock.Object, ratesMock.Object, transMock.Object, tipCalculation, currencyConversor);
            var response = service.callService("AA2223", "EUR").Result as BadRequestResult;

            Assert.Equal(400, response.StatusCode);
        }
    }
}
