using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Controllers;
using TIPS_CALCULATOR.Services.Interfaces;
using Xunit;

namespace TIPS_TEST
{
    public class TipsControllerTest
    {
        [Fact]
        public void TestGetRates()
        {
            Mock<IRatesService> moqRates = new Mock<IRatesService>();
            Mock<ITransactionsService> moqTransactions = new Mock<ITransactionsService>();
            Mock<ICalculatorService> moqCalculator = new Mock<ICalculatorService>();

            moqRates.Setup(x => x.getRates()).Returns(Task.FromResult(new OkResult() as IActionResult));

            TipsController controller = new TipsController(moqRates.Object, moqTransactions.Object, moqCalculator.Object);
            var response = controller.GetRates().Result as OkResult;
            Assert.Equal(200, response.StatusCode);
        }

        [Fact]
        public void TestGetTransactions()
        {
            Mock<IRatesService> moqRates = new Mock<IRatesService>();
            Mock<ITransactionsService> moqTransactions = new Mock<ITransactionsService>();
            Mock<ICalculatorService> moqCalculator = new Mock<ICalculatorService>();

            moqTransactions.Setup(x => x.getTransactions()).Returns(Task.FromResult(new OkResult() as IActionResult));
            TipsController controller = new TipsController(moqRates.Object, moqTransactions.Object, moqCalculator.Object);
            var response = controller.GetTransactions().Result as OkResult;

            Assert.Equal(200, response.StatusCode);

        }

        [Fact]
        public void TestGetTips()
        {
            Mock<IRatesService> moqRates = new Mock<IRatesService>();
            Mock<ITransactionsService> moqTransactions = new Mock<ITransactionsService>();
            Mock<ICalculatorService> moqCalculator = new Mock<ICalculatorService>();

            moqCalculator.Setup(x => x.callService(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(new OkResult() as IActionResult));
            TipsController controller = new TipsController(moqRates.Object, moqTransactions.Object, moqCalculator.Object);
            var response = controller.GetTips("sku","currency").Result as OkResult;

            Assert.Equal(200, response.StatusCode);
        }
    }
}
