﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Services.Interfaces;

namespace TIPS_CALCULATOR.Controllers
{
    [Route("api/[controller]")]
    public class TipsController : Controller
    {
        private readonly IRatesService _ratesService;
        private readonly ITransactionsService _trasactionService;
        private readonly ICalculatorService _calculatorService;

        public TipsController(IRatesService ratesService, ITransactionsService transactionService, ICalculatorService calculatorService)
        {
            this._ratesService = ratesService;
            this._trasactionService = transactionService;
            this._calculatorService = calculatorService;
        }

        //** GET: api/rates
        [HttpGet("Rates")]
        public async Task<IActionResult> GetRates()
        {
            return await _ratesService.getRates();
        }

        //** GET: api/transactions
        [HttpGet("Transactions")]
        public async Task<IActionResult> GetTransactions()
        {
            return await _trasactionService.getTransactions();
        }

        //** GET: api/Tips/SKU/CURRENCY
        [HttpGet ("{sku}/{currency}", Name = "Calculate")]
        public async Task<IActionResult> GetTips(string sku, string currency)
        {
            return await _calculatorService.callService(sku, currency);
        }
    }
}
