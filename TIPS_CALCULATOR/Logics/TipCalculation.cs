﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public class TipCalculation : ITipCalculation
    {
        public List<Tip> calculate(List<Transaction> transactions)
        {
            List<Tip> tips = transactions.Select(t => new Tip(t)).ToList();
            foreach(Tip tip in tips)
            {
                tip.tip = Math.Round((tip.amount * 0.05m * 1000)/1000, 2, MidpointRounding.ToEven);
            }

            return tips;
        }
    }
}
