﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics
{
    public class CurrencyConversor : ICurrencyConversor
    {
        public Tip convert(string currency, List<Tip> tips, List<Rate> rates)
        {
            //decimal tipConverted = 0;
            Tip tipConverted = new Tip { sku = "TOTAL", currency = currency};

            foreach (Tip tip in tips)
            {
                if(tip.currency.Equals(currency)) {
                    tipConverted.amount += tip.amount;
                    tipConverted.tip += tip.tip;
                }
                else
                {
                    var rateToConvert = rates.Where(x => x.from == tip.currency && x.to == currency).FirstOrDefault();
                    if (rateToConvert != null)
                    {
                        tipConverted.amount += tip.amount * rateToConvert.rate;
                        tipConverted.tip += tip.tip * rateToConvert.rate;
                    }
                    else
                    {
                        var ratesFrom = rates.Where(x => x.from == tip.currency).ToList();

                        foreach (Rate rate in ratesFrom)
                        {
                            var rateTo = rates.Where(x => x.from == rate.to && x.to == currency).FirstOrDefault();
                            if(rateTo != null)
                            {
                                decimal firstConversionAmount = tip.amount * rate.rate;
                                decimal firstConversionTip = tip.tip * rate.rate;

                                tipConverted.amount += firstConversionAmount * rateTo.rate;
                                tipConverted.tip += firstConversionTip * rateTo.rate;
                                break;
                            }
                        }
                    }
                }
            }

            tipConverted.amount = Math.Round((tipConverted.amount * 1000) / 1000, 2, MidpointRounding.ToEven);
            tipConverted.tip = Math.Round((tipConverted.tip * 1000) / 1000, 2, MidpointRounding.ToEven);
            return tipConverted;
        }
    }
}
