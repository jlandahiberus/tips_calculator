﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics
{
    public class ParseToRate : IParseToRate
    {
        public List<Rate> parseRates(string value)
        {
            List<Rate> list = JsonConvert.DeserializeObject<List<Rate>>(value);
            return list;
        }
    }
}
