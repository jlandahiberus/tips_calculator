﻿using System.Collections.Generic;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public interface IParseToTransaction
    {
        List<Transaction> parseTransactions(string value);
    }
}
