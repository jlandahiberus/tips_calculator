﻿using System.Collections.Generic;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public interface IParseToRate
    {
        List<Rate> parseRates(string value);
    }
}
