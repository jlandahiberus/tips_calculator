﻿using System.Collections.Generic;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public interface ITipCalculation
    {
        List<Tip> calculate(List<Transaction> transactions);
    }
}
