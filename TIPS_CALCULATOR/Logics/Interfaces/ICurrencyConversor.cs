﻿using System.Collections.Generic;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public interface ICurrencyConversor
    {
        Tip convert(string currency, List<Tip> tips, List<Rate> rates);
    }
}
