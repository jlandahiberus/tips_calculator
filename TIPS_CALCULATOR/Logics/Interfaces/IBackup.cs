﻿namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public interface IBackup
    {
       void writeBackup(string value);

       string readBackup();

       void setFileSection(string fileSection);
    }
}
