﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TIPS_CALCULATOR.Models;
using TIPS_CALCULATOR.Logics.Interfaces;

namespace TIPS_CALCULATOR.Logics
{
    public class ParseToTransaction : IParseToTransaction
    {
        public List<Transaction> parseTransactions(string value)
        {
            List<Transaction> list = JsonConvert.DeserializeObject<List<Transaction>>(value);
            return list;
        }
    }
}
