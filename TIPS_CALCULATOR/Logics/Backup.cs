﻿using Microsoft.Extensions.Logging;
using System.IO;
using TIPS_CALCULATOR.Configurations.Interfaces;

namespace TIPS_CALCULATOR.Logics.Interfaces
{
    public class Backup : IBackup
    {
        private readonly IBackupConfig _backupConfig;
        private readonly ILogger<Backup> _logger;
        private string _fileSection;

        public Backup(IBackupConfig backupConfig, ILogger<Backup> logger)
        {
            this._backupConfig = backupConfig;
            this._logger = logger;
        }

        public void writeBackup(string value)
        {
            string dir = _backupConfig._backupConfiguration.directory;

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            dir = dir + _backupConfig._backupConfiguration.file;

            _logger.LogInformation("Writting in backup file: " + dir);
            using (StreamWriter writer = File.CreateText(dir))
            {
                writer.Write(value);
                _logger.LogInformation("Writted ==>" + value);
            }
        }

        public string readBackup()
        {
            string dir = _backupConfig._backupConfiguration.directory + _backupConfig._backupConfiguration.file;

            _logger.LogInformation("Reading from backup file: " + dir);
            if (File.Exists(dir))
            {
                return File.ReadAllText(dir);
            }
            else
            {
                _logger.LogWarning("Backup file doesn't exist, Values can't be load");
                return "ERROR";
            }
        }

        public void setFileSection(string fileSection)
        {
            this._fileSection = fileSection;
            _backupConfig.setConfigSection(_fileSection);
        }
    }
}
