﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Services.Interfaces;

namespace TIPS_CALCULATOR.Services
{
    public class ClientService : IClientService
    {
        private readonly ILogger<ClientService> _logger;

        public ClientService(ILogger<ClientService> logger)
        {
            this._logger = logger;
        }

        public async Task<IActionResult> callService(string url, string endpoint, string contentType)
        {
            string connectionString = url + endpoint;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue(contentType));
            client.Timeout = TimeSpan.FromSeconds(15);

            _logger.LogInformation("Trying to connect : " + connectionString);
            try
            {
                var response = await client.GetAsync(connectionString, HttpCompletionOption.ResponseHeadersRead);

                if (response.IsSuccessStatusCode)
                {
                    _logger.LogInformation("Connection established | Trying to GET JSON");
                    var json = await response.Content.ReadAsStringAsync();

                    _logger.LogInformation("Connection Result ==> " + json);
                    return new OkObjectResult(json);
                }
                else
                {
                    _logger.LogWarning("Connection Error | Error return to load Backup");
                    return new BadRequestResult();
                }

            }
            catch (TaskCanceledException ex)
            {
                _logger.LogWarning("Timeout Connection", ex);
                return new BadRequestObjectResult("Timeout");
            }
            catch (Exception e)
            {
                _logger.LogWarning("Connection Error | Error return for load Backup", e);
                return new BadRequestResult();
            }
        }
    }
}
