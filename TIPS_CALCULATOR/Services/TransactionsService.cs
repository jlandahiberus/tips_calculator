﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Services.Interfaces;

namespace TIPS_CALCULATOR.Services
{
    public class TransactionsService : ITransactionsService
    {
        private readonly string _configSection = "Transactions";

        private readonly IClientService _clientService;
        private readonly ILogger<TransactionsService> _logger;
        private readonly IServiceConfig _serviceConfig;
        private readonly IBackup _backup;
        private readonly IParseToTransaction _parseToTransaction;

        public TransactionsService(IClientService clientService, ILogger<TransactionsService> logger, IServiceConfig serviceConfig, IBackup backup, IParseToTransaction parseToTransaction)
        {
            this._clientService = clientService;
            this._logger = logger;
            this._serviceConfig = serviceConfig;
            this._backup = backup;
            this._parseToTransaction = parseToTransaction;
        }

        public async Task<IActionResult> getTransactions()
        {
            _logger.LogTrace("Calling GET TRANSACTIONS as a WEB SERVICES");
            this._serviceConfig.setConfigSection(_configSection);
            this._backup.setFileSection(_configSection);

            var responseTransactions = await _clientService.callService(_serviceConfig._serviceConfiguration.url
               , _serviceConfig._serviceConfiguration.endpoint
               , _serviceConfig._serviceConfiguration.contentType);

            if (responseTransactions is OkObjectResult result)
            {
                _backup.writeBackup(result.Value.ToString());
                var value = _parseToTransaction.parseTransactions(result.Value.ToString());
                _logger.LogTrace("TRANSACTIONS successful");
                return new OkObjectResult(value);
            }
            else
            {
                var valueBackup = _backup.readBackup();
                if (!valueBackup.Equals("ERROR"))
                {
                    var value = _parseToTransaction.parseTransactions(valueBackup);
                    _logger.LogTrace("TRANSACTIONS error, returning Backup results");
                    return new OkObjectResult(value);
                }
                else{
                    return new BadRequestResult();
                }
            }
        }

        public async Task<IActionResult> getTransactionsBySku(string sku)
        {
            _logger.LogTrace("Calling GET TRANSACTIONS as a WEB SERVICES");
            this._serviceConfig.setConfigSection(_configSection);
            this._backup.setFileSection(_configSection);

            var responseTransactions = await _clientService.callService(_serviceConfig._serviceConfiguration.url
               , _serviceConfig._serviceConfiguration.endpoint
               , _serviceConfig._serviceConfiguration.contentType);

            if (responseTransactions is OkObjectResult result)
            {
                var value = _parseToTransaction.parseTransactions(result.Value.ToString()).Where(x => x.sku == sku).ToList();
                if(value.Count() == 0)
                {
                    _logger.LogTrace("No found sku: " + sku + "in results, locking in to backup");
                    var valueBackup = _backup.readBackup();
                    if (!valueBackup.Equals("ERROR"))
                    {
                        value = _parseToTransaction.parseTransactions(valueBackup).Where(x => x.sku == sku).ToList();
                    }
                }

                _backup.writeBackup(result.Value.ToString());
                _logger.LogTrace("TRANSACTIONS successful");
                return new OkObjectResult(value);
            }
            else
            {
                var valueBackup = _backup.readBackup();
                if (!valueBackup.Equals("ERROR"))
                {
                    var value = _parseToTransaction.parseTransactions(valueBackup).Where(x => x.sku == sku).ToList();
                    _logger.LogTrace("TRANSACTIONS error, returning Backup results");
                    return new OkObjectResult(value);
                }
                else
                {
                    _logger.LogTrace("BACKUP can't be read, returning error");
                    return new BadRequestResult();
                }
            }
        }
    }
}
