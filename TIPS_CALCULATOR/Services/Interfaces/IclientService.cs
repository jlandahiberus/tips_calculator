﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TIPS_CALCULATOR.Services.Interfaces
{
    public interface IClientService
    {
        Task<IActionResult> callService(string url, string endpoint, string contentType);
    }
}
