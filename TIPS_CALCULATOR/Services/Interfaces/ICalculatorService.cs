﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TIPS_CALCULATOR.Services.Interfaces
{
    public interface ICalculatorService
    {
        Task<IActionResult> callService(string sku, string currency);
    }
}
