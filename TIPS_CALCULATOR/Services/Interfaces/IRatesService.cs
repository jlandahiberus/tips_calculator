﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TIPS_CALCULATOR.Services.Interfaces
{
    public interface IRatesService
    {
        Task<IActionResult> getRates();
    }
}
