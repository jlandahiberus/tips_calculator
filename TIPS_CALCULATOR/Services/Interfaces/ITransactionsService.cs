﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TIPS_CALCULATOR.Services.Interfaces
{
    public interface ITransactionsService
    {
        Task<IActionResult> getTransactions();

        Task<IActionResult> getTransactionsBySku(string sku);
    }
}
