﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Services.Interfaces;

namespace TIPS_CALCULATOR.Services
{
    public class RatesService : IRatesService
    {
        private readonly string _configSection = "Rates";

        private readonly IClientService _clientService;
        private readonly ILogger<RatesService> _logger;

        private readonly IServiceConfig _serviceConfig;
        private readonly IParseToRate _parseToRate;
        private readonly IBackup _backup;

        public RatesService(IClientService clientService, ILogger<RatesService> logger, IServiceConfig serviceConfig, IBackup backup, IParseToRate parseToRate)
        {
            this._clientService = clientService;
            this._logger = logger;
            this._serviceConfig = serviceConfig;
            this._backup = backup;
            this._parseToRate = parseToRate;
        }

        public async Task<IActionResult> getRates()
        {
            _logger.LogTrace("Calling GET RATES as a WEB SERVICES");
            this._serviceConfig.setConfigSection(_configSection);
            this._backup.setFileSection(_configSection);

            var responseRates = await _clientService.callService(_serviceConfig._serviceConfiguration.url
               , _serviceConfig._serviceConfiguration.endpoint
               , _serviceConfig._serviceConfiguration.contentType);

            if (responseRates is OkObjectResult result)
            {
                _backup.writeBackup(result.Value.ToString());
                var value = _parseToRate.parseRates(result.Value.ToString());
                _logger.LogTrace("RATES successful");
                return new OkObjectResult(value);
            }
            else
            {
                var valueBackup = _backup.readBackup();
                if (!valueBackup.Equals("ERROR"))
                {
                    var value = _parseToRate.parseRates(valueBackup);
                    _logger.LogTrace("RATES error, returning Backup results");
                    return new OkObjectResult(value);
                }
                else
                {
                    _logger.LogTrace("BACKUP can't be read, returning error");
                    return new BadRequestResult();
                }
                
            }
        }
    }
}
