﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Models;
using TIPS_CALCULATOR.Services.Interfaces;

namespace TIPS_CALCULATOR.Services
{
    public class CalculatorService : ICalculatorService
    {
        private readonly ILogger<CalculatorService> _logger;
        private readonly IRatesService _ratesService;
        private readonly ITransactionsService _transactionService;
        private readonly ITipCalculation _tipCalculation;
        private readonly ICurrencyConversor _currencyConversor;

        public CalculatorService(ILogger<CalculatorService> logger, IRatesService ratesService, ITransactionsService transactionService, ITipCalculation tipCalculation, ICurrencyConversor currencyConversor)
        {
            this._logger = logger;
            this._ratesService = ratesService;
            this._transactionService = transactionService;
            this._tipCalculation = tipCalculation;
            this._currencyConversor = currencyConversor;
        }

        public async Task<IActionResult> callService(string sku, string currency)
        {
            var responseRates = await _ratesService.getRates();

            if(responseRates is OkObjectResult resultRates)
            {
                var responseTransactions = await _transactionService.getTransactionsBySku(sku);

                if(responseTransactions is OkObjectResult resultTransactions)
                {
                    var TipsCalculated = _tipCalculation.calculate((List<Transaction>)resultTransactions.Value);
                    var TipConverted = _currencyConversor.convert(currency, TipsCalculated, (List<Rate>)resultRates.Value);

                    TipsCalculated.Insert(0, TipConverted);
                    return new OkObjectResult(TipsCalculated);
                }
                else
                {
                    return new BadRequestResult();
                }
            }
            else
            {
                return new BadRequestResult();
            }
        }
    }
}
