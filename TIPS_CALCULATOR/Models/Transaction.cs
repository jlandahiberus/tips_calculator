﻿
namespace TIPS_CALCULATOR.Models
{
    public class Transaction
    {
        public string sku { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
    }
}
