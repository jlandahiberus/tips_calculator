﻿
namespace TIPS_CALCULATOR.Models
{
    public class Rate
    {
        public string from { get; set; }
        public string to { get; set; }
        public decimal rate { get; set; }
    }
}
