﻿
namespace TIPS_CALCULATOR.Models
{
    public class ServiceConfiguration
    {
        public string url { get; set; }
        public string endpoint { get; set;}
        public string contentType { get; set; }
    }
}
