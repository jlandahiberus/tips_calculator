﻿
namespace TIPS_CALCULATOR.Models
{
    public class Tip : Transaction
    {
        public decimal tip { get; set; }

        public Tip() { }

        public Tip(Transaction t)
        {
            this.sku = t.sku;
            this.currency = t.currency;
            this.amount = t.amount;
        }
    }
}
