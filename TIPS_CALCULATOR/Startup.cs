﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using TIPS_CALCULATOR.Services;
using TIPS_CALCULATOR.Services.Interfaces;
using TIPS_CALCULATOR.Configurations;
using TIPS_CALCULATOR.Logics;
using TIPS_CALCULATOR.Logics.Interfaces;
using TIPS_CALCULATOR.Configurations.Interfaces;

namespace TIPS_CALCULATOR
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ICalculatorService, CalculatorService>();
            services.AddScoped<IRatesService, RatesService>();
            services.AddScoped<ITransactionsService, TransactionsService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IServiceConfig, ServiceConfig>();
            services.AddScoped<IBackupConfig, BackupConfig>();
            services.AddScoped<IParseToRate, ParseToRate>();
            services.AddScoped<IParseToTransaction, ParseToTransaction>();
            services.AddScoped<ITipCalculation, TipCalculation>();
            services.AddScoped<ICurrencyConversor, CurrencyConversor>();
            services.AddScoped<IBackup, Backup>();

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            env.ConfigureNLog("nlog.config");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddNLog();
            app.AddNLogWeb();
            app.UseMvc();
        }
    }
}
