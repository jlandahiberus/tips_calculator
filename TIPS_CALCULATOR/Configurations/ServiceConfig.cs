﻿using Microsoft.Extensions.Configuration;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Configurations
{
    public class ServiceConfig : IServiceConfig
    {
        private readonly IConfiguration _config;
        public ServiceConfiguration _serviceConfiguration { get; }

        public ServiceConfig(IConfiguration config)
        {
            this._config = config;
            this._serviceConfiguration = new ServiceConfiguration();
        }

        public void setConfigSection(string configSection)
        {
            this._serviceConfiguration.contentType = _config.GetSection(configSection + ":Service:Content-Type").Value;
            this._serviceConfiguration.url = _config.GetSection(configSection + ":Service:Route").Value;
            this._serviceConfiguration.endpoint = _config.GetSection(configSection + ":Service:endPoint").Value;
        }
    }
}
