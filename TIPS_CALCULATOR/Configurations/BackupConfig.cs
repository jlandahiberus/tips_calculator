﻿using Microsoft.Extensions.Configuration;
using TIPS_CALCULATOR.Configurations.Interfaces;
using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Configurations
{
    public class BackupConfig : IBackupConfig
    {
        private readonly IConfiguration _config;
        public BackupConfiguration _backupConfiguration { get; }

        public BackupConfig(IConfiguration config)
        {
            this._config = config;
            this._backupConfiguration = new BackupConfiguration();
        }

        public void setConfigSection(string configSection)
        {
            this._backupConfiguration.directory = _config.GetSection(configSection + ":Backup:Dir").Value;
            this._backupConfiguration.file = _config.GetSection(configSection + ":Backup:File").Value;
        }
    }
}
