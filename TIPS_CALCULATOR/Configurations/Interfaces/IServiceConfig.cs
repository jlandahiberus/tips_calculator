﻿using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Configurations.Interfaces
{
    public interface IServiceConfig
    {
        ServiceConfiguration _serviceConfiguration { get; }

        void setConfigSection(string configSection);
    }
}
