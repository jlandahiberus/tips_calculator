﻿using TIPS_CALCULATOR.Models;

namespace TIPS_CALCULATOR.Configurations.Interfaces
{
    public interface IBackupConfig
    {
        BackupConfiguration _backupConfiguration { get; }

        void setConfigSection(string configSection);
    }
}
